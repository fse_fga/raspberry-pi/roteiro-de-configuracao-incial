# Roteiro de Configuração Incial - Raspberry Pi

Este é um roteiro de configuração das placas Raspberry Pi 3 para exercícios em sala da disciplina de FSE (Fundamentos do Sistemas Embarcados)

## Pré-requisitos

Instalar o programa `Raspberry Pi Imager` à partir do site oficial [raspberrypi.com](https://www.raspberrypi.com/software/).

![](https://assets.raspberrypi.com/static/md-bfd602be71b2c1099b91877aed3b41f0.png)

## Configuração da Imagem

1. Abrir o `Raspberry Pi Imager` e selecionar a imagem do `Raspberry Pi OS (32-bits)`
2. No ícone da engrenagem, configurar os seguintes itens:  
   1. `hostname` : `rpi3-<NUMERO_DA_PLACA>`. Para o `NUMERO_DA_PLACA` utiliza o número de dois digitos marcado na caixa da placa.
   2. `Enable SSH` with password authentication.
<br>
<br>

![](imagens/rpi3-hostname_ssh.png)

   3. Configure o usuário e senha como:  
      1. `username`: `pi`
      2. `password`: `raspberry`

<br>

![](imagens/rpi3-username_password.png)

   4. Configurar as credencias de Wifi:  

<br>

![](imagens/rpi3-wifi.png)

   5. Configurar a localização:  

<br>

![](imagens/rpi3-locale.png)

## Gravação da Imagem

1. Insira o **SDCard** através do adaptador USB no computador.
2. Clique no botão `Choose Storage` e selecione o **SDCard** e prossiga com o comando `Write`.

## Iniciar e fazer acesso remoto à Raspberry Pi

1. Insira o SDCard na Raspberry Pi.
2. Connecte o cabo de energia (Micro USB) para ligar a placa.
3. Realizar o acesso remoto via SSH

```
ssh pi@rpi3-<NUMERO_DA_PLACA>.local
```

## Como desligar a placa

```
sudo poweroff
```

